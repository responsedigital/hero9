module.exports = {
    'name'  : 'Hero9',
    'camel' : 'Hero9',
    'slug'  : 'hero-9',
    'dob'   : 'Hero_9_1440',
    'desc'  : 'A hero component with a play button and background image',
}