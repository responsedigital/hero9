<?php

namespace Fir\Pinecones\Hero9;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Hero9',
            'label' => 'Pinecone: Hero9',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A hero component with a backgorund color/image and large text. Optional scroll below icon"
                ],
                [
                    'label' => 'Text ',
                    'name' => 'textTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text'
                ],
                [
                    'label' => 'SubTitle',
                    'name' => 'subtitle',
                    'type' => 'text'
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'textarea'
                ],
                [
                    'label' => 'CTA ',
                    'name' => 'ctaTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'CTA',
                    'name' => 'cta',
                    'type' => 'link'
                ],
                [
                    'label' => 'Image',
                    'name' => 'imageTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image'
                ],          
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID',
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                // color
                [
                    'label' => 'Color Overlay',
                    'name' => 'overlay',
                    'type' => 'color_picker'
                ],
                [
                    'label' => 'Color Blending',
                    'name' => 'blending',
                    'type' => 'radio',
                    'choices' => array(
                        'default' => 'Default',
                        'multiply' => 'Multiply',
                        'screen' => 'Screen',
                        'overlay' => 'Overlay',
                        'darken' => 'Darken',
                        'lighten' => 'Lighten',
                        'color-dodge' => 'Color-Dodge',
                        'color-burn' => 'Color-Burn',
                        'difference' => 'Difference',
                        'exclusion' => 'Exclusion',
                        'hue' => 'Hue',
                        'saturation' => 'Saturation',
                        'color' => 'Color',
                        'luminosity' => 'Luminosity'
                    ),
                ],
                [
                    'label' => 'Overlay Opacity',
                    'name' => 'opacity',
                    'type' => 'range'
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        [
                            'label' => 'Show Play Button Arrow?',
                            'name' => 'show_play',
                            'type' => 'true_false',
                            'message' => 'Yes, show the playbutton',
                            'default_value' => 1,
                            'ui' => 1   
                        ],
                        GlobalFields::getOptions(array(

                        ))         
                    ]
                ]              
            ]
        ];
    }
}