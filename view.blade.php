<!-- Start Hero9 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A hero component with a backgorund color/image and large text. Optional Play Button -->
@endif
<div class="hero-9"  is="fir-hero-15">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="hero-9__wrap">

    @if($image)
    {{-- <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}" class="hero-9__img"> --}}
    <picture class="hero-9__img" id="hero__img--src">
      <!--[if IE 9]><video style="display: none;"><![endif]-->
      <source media="(max-width: 599px)" srcset="{{ Fir\Utils\Images::transform_image($image['url'], 'hero', 'small') }}" class=' hero__img--src hero__img--sm'>
      <source media="(max-width: 1023px)" srcset="{{ Fir\Utils\Images::transform_image($image['url'], 'hero', 'medium') }}" class='hero__img--src hero__img--md'>
      <source media="(min-width: 1024px)" srcset="{{ Fir\Utils\Images::transform_image($image['url'], 'hero', 'large') }}" class='hero__img--src hero__img--lg'>
      <!--[if IE 9]></video><![endif]-->
      <img alt="{{ $image['alt'] }}" src="{{ Fir\Utils\Images::transform_image($image['url'], 'hero', 'small') }}"  class='hero__img--xl'>  
    </picture>

    @endif

    @if($overlay && $opacity)
      <div class="hero-9__overlay" style="{{$overlay}} {{$blending}} {{$opacity}}"></div>
    @endif

    <div class="hero-9__copy">
      @if($text)
        <h3 class="hero-9__subtitle">
            {!! Fir\Utils\Helpers::convertPunctuation($title) !!}
        </h3>
      @endif

      <h1 class="hero-9__title">
          {!! Fir\Utils\Helpers::convertPunctuation($title) !!}
      </h1>

      @if($text)
        <h3 class="hero-9__text">
          {!! Fir\Utils\Helpers::convertPunctuation($text) !!}
        </h3>
      @endif
  
      @if($cta)
      <a href="{{ $cta['url'] }}" class="btn">
          {{ $cta['title'] }}
      </a>
      @endif
    </div>


    @if(!$options['show_play'])
    <div style="visibility:hidden; pointer-events: none;">
    @endif

    <svg class="hero-03__play" data-play xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" aria-labelledby="title"
    aria-describedby="desc" role="img" xmlns:xlink="http://www.w3.org/1999/xlink">
    <title>Play</title>
    <desc>A solid styled icon from Orion Icon Library.</desc>
    <path data-name="layer1"
    d="M32 2a30 30 0 1 0 30 30A30 30 0 0 0 32 2zm-8 44V18l24 14z" fill="#202020"></path>
    </svg>
    @if(!$component['options']['show_scroller'])
    </div>
    @endif

  </div>
</div>
<!-- End Hero9 -->