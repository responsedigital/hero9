class Hero9 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initHero9()
    }

    initHero9 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Hero9")
    }

}

window.customElements.define('fir-hero-9', Hero9, { extends: 'div' })
